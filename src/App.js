import logo from './logo.svg';
import './App.css';

const App = () => {
  const x = 100;
  const y = 20;

  const a = do {
    if (x > 10) {
      if (y > 20) {
        ("big x, big y");
      } else {
        ("big x, small y");
      }
    } else {
      if (y > 10) {
        ("small x, big y");
      } else {
        ("small x, small y");
      }
    }
  };

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <div>My name is Vitalii Vaskivskyi</div>
        <div>I want to imporove my skills at .NET and React direction</div>
        <div>I want to pass sertificate at ITVDN platform</div>
        <div>Arguments: because when I pass courses and get some sertificats from that platform it will boost my knowladge and skills which will help me at my job as developer</div>
        <div>Exprerimental feature do expresion: {a}</div>
      </header>
    </div>
  );
}

export default App;
